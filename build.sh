#!/bin/bash


build() {
  echo "Building Godot Engine..."

}


test() {
  echo "Running tests..."

}


package() {
  echo "Packaging or deploying..."
  
}


if [ "$1" == "ci" ]; then

  case "$2" in
    "build")
      build
      ;;
    "test")
      test
      ;;
    "package")
      package
      ;;
    *)
      echo "Invalid CI stage"
      exit 1
      ;;
  esac
else
  
  echo "Running standalone build..."
  build
  
fi
